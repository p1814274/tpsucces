<?php

namespace App\Controller;

use App\Form\ArticleType;
use App\Entity\Article;
use App\Entity\Liste;
use App\Form\ListeType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ListeController extends AbstractController
{
    #[Route('/liste', name: 'app_liste')]
    public function index(): Response
    {
        return $this->render('liste/index.html.twig', [
            'controller_name' => 'ListeController',
        ]);
    }

    #[Route('/liste/nouveau', name: 'app_liste_nouveau')]
    public function nouveau(Request $request, EntityManagerInterface $entityManager): Response
    {
        $liste = new Liste();

        $form = $this->createForm(ListeType::class, $liste);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($liste);
            $entityManager->flush();

            return $this->redirectToRoute('app_home');
        }

        return $this->render('liste/nouveau.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    #[Route('/', name: 'app_home')]
    public function home(EntityManagerInterface $entityManager): Response
    {
        $listes = $entityManager->getRepository(Liste::class)->findAll();

        return $this->render('liste/home.html.twig', [
            'listes' => $listes,
        ]);
    }

    #[Route('/liste/{id}', name: 'app_liste_afficher')]
    public function afficher(Request $request, EntityManagerInterface $entityManager, $id): Response
    {
        $liste = $entityManager->getRepository(Liste::class)->find($id);

        if (!$liste) {
            throw $this->createNotFoundException('La liste n\'existe pas');
        }

        $article = new Article();
        $article->setListe($liste);

        $form = $this->createForm(ArticleType::class, $article);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($article);
            $entityManager->flush();

            return $this->redirectToRoute('app_liste_afficher', ['id' => $id]);
        }

        $articles = $entityManager->getRepository(Article::class)->findBy(['liste' => $id]);

        return $this->render('liste/afficher.html.twig', [
            'liste' => $liste,
            'articles' => $articles,
            'form' => $form->createView(),
        ]);
    }
    #[Route('/liste/{id}/modifier', name: 'app_liste_modifier')]
    public function modifier(Request $request, EntityManagerInterface $entityManager, $id): Response
    {
        $liste = $entityManager->getRepository(Liste::class)->find($id);

        $form = $this->createForm(ListeType::class, $liste);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('app_home');
        }

        return $this->render('liste/modifier.html.twig', [
            'form' => $form->createView(),
            'liste' => $liste,
        ]);
    }

    #[Route('/liste/{id}/supprimer', name: 'app_liste_supprimer')]
    public function supprimer(Request $request, EntityManagerInterface $entityManager, $id): Response
    {
        $liste = $entityManager->getRepository(Liste::class)->find($id);

        if (!$liste) {
            throw $this->createNotFoundException('La liste n\'existe pas.');
        }

        $entityManager->remove($liste);
        $entityManager->flush();

        return $this->redirectToRoute('app_home');
    }

}

