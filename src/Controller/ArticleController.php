<?php

namespace App\Controller;

use App\Entity\Liste;
use App\Entity\Article;
use App\Form\ArticleType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ArticleController extends AbstractController
{
    #[Route('/article', name: 'app_article')]
    public function index(): Response
    {
        return $this->render('article/index.html.twig', [
            'controller_name' => 'ArticleController',
        ]);
    }

    #[Route('/liste/{id}/article/nouveau', name: 'app_article_nouveau')]
    public function nouveau(Request $request, EntityManagerInterface $entityManager, $id): Response
    {
        $article = new Article();

        $form = $this->createForm(ArticleType::class, $article);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $liste = $entityManager->getRepository(Liste::class)->find($id);
            $article->setListe($liste);
            $entityManager->persist($article);
            $entityManager->flush();

            return $this->redirectToRoute('app_liste_afficher', ['id' => $id]);
        }

        return $this->render('article/nouveau.html.twig', [
            'form' => $form->createView(),
        ]);
    }
    #[Route('/liste/{id}/article/ajouter', name: 'app_article_ajouter')]
    public function ajouter(Request $request, EntityManagerInterface $entityManager, $id): Response
    {
        $liste = $entityManager->getRepository(Liste::class)->find($id);

        $article = new Article();
        $article->setListe($liste);

        $form = $this->createForm(ArticleType::class, $article);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($article);
            $entityManager->flush();

            return $this->redirectToRoute('app_liste_afficher', ['id' => $id]);
        }

        return $this->render('article/ajouter.html.twig', [
            'form' => $form->createView(),
            'liste' => $liste,
        ]);
    }

    #[Route('/article/{id}/modifier', name: 'app_article_modifier')]
    public function modifier(Request $request, EntityManagerInterface $entityManager, $id): Response
    {
        $article = $entityManager->getRepository(Article::class)->find($id);

        $form = $this->createForm(ArticleType::class, $article);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('app_liste_afficher', ['id' => $article->getListe()->getId()]);
        }

        return $this->render('article/modifier.html.twig', [
            'form' => $form->createView(),
            'article' => $article,
            'liste' => $article->getListe(), // Ajouter la variable 'liste'
        ]);
    }


    #[Route('/article/{id}/supprimer', name: 'app_article_supprimer')]
    public function supprimer(Request $request, EntityManagerInterface $entityManager, $id): Response
    {
        $article = $entityManager->getRepository(Article::class)->find($id);

        if (!$article) {
            throw $this->createNotFoundException('L\'article n\'existe pas.');
        }

        $entityManager->remove($article);
        $entityManager->flush();

        return $this->redirectToRoute('app_liste_afficher', ['id' => $article->getListe()->getId()]);
    }
    #[Route('/valider-achat-article/{id}', name: 'valider_achat_article')]
    public function validerAchatArticle(Request $request, Article $article): Response
    {
        $form = $this->createFormBuilder($article)
            ->add('achete', CheckboxType::class)
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $article = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($article);
            $em->flush();

            $this->addFlash('success', 'L\'achat de l\'article a été validé.');
        }

        return $this->redirectToRoute('app_liste_afficher', ['id' => $article->getListe()->getId()]);
    }

    #[Route('/liste/{id}/article/plus-cher', name: 'app_article_plus_cher')]
    public function afficherPlusCher(EntityManagerInterface $entityManager, $id): Response
    {
        $repository = $entityManager->getRepository(Article::class);
        $articles = $repository->findBy(['liste' => $id]);

        $articlePlusCher = null;
        $prixMax = 0;

        foreach ($articles as $article) {
            if ($article->getPrix() > $prixMax) {
                $articlePlusCher = $article;
                $prixMax = $article->getPrix();
            }
        }

        return $this->render('liste/afficher.html.twig', [
            'articlePlusCher' => $articlePlusCher,
        ]);
        
    }





}

